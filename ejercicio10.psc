Algoritmo sin_titulo
	// Ejercicio 10
	Leer n1
	Si n1<5 Entonces
		Escribir 'suspenso'
	SiNo
		Si n1<6 Entonces
			Escribir 'suficiente'
		SiNo
			Si n1<7 Entonces
				Escribir 'bien'
			SiNo
				Si n1<9 Entonces
					Escribir 'notable'
				SiNo
					Si n1<10 Entonces
						Escribir 'sobresaliente'
					FinSi
				FinSi
			FinSi
		FinSi
	FinSi
FinAlgoritmo

